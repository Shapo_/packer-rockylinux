# TP Rocky Linux x Packer

## Prérequis 

```
yum install ansible ansible-collection-ansible-posix -y
```

## Clonage du répertoire configuré

```
git clone https://gitlab.com/Shapo_/packer-rockylinux.git
```

## Lancer le build 

```
packer build rocky-8.pkr.hcl
```

## Lancer la VM

```
ssh -L 5999:127.0.0.1:5999 root@192.168.1.50
```

### Vérification du build 

```
==> Wait completed after 15 minutes 52 seconds

==> Builds finished. The artifacts of successful builds are:
--> qemu.example: VM files in directory: build-rocky-8
```
